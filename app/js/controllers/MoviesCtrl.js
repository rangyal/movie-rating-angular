function MoviesCtrl(MoviesService) {
  'ngInject';

  const vm = this;

  activate();

  function activate() {
    MoviesService.getMovies()
      .then((movies) => vm.list = movies)
      .catch(() => vm.serverError = true);
  }
}

export default {
  name: 'MoviesCtrl',
  fn: MoviesCtrl
};
