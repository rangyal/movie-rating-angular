import _ from 'lodash';

/**
 * @name rating
 * @restrict E
 * @description Rating component with configurable number of stars
 * @param {number} value Angular expression to data-bind to
 * @param {number=} maxValue Number of the stars to display
 * @example
    <rating max-value="5" value="movie.rating"></rating>
 */
function Rating($timeout) {
  'ngInject';

  return {
    restrict: 'E',
    templateUrl: 'directives/rating.html',
    scope: {
      value: '='
    },
    link: function(scope, element, attrs) {

      scope.animating = false;
      scope.values    = _.range(1, parseInt(attrs.maxValue) + 1);
      scope.select    = select;

      function select(value) {
        if (scope.animating) return;

        scope.value     = value;
        scope.animating = true;

        $timeout(() => scope.animating = false, 500 * value);
      }
    }
  };

}

export default {
  name: 'rating',
  fn: Rating
};
