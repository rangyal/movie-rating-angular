const AppSettings = {
  appTitle: 'Movie Rating',
  moviesServiceUrl: 'https://gist.githubusercontent.com/razh/c37558e72a30bd836ec8f5192506f54a/raw/158671f939a695289685e8c3dd420ead4684ebda/movies.json'
};

export default AppSettings;
