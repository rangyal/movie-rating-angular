function OnConfig($stateProvider, $locationProvider, $urlRouterProvider, $compileProvider) {
  'ngInject';

  if (process.env.NODE_ENV === 'production') {
    $compileProvider.debugInfoEnabled(false);
  }

  $stateProvider
  .state('movies', {
    url: '/',
    controller: 'MoviesCtrl as movies',
    templateUrl: 'movies.html',
    title: 'Movies'
  });

  $urlRouterProvider.otherwise('/');

}

export default OnConfig;
