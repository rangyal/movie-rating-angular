/**
 * @name MoviesService
 * @desc Operations with Movies
 */
function MoviesService($http, AppSettings) {
  'ngInject';

  /**
   * @name getMovies
   * @desc Returns movies
   * @returns {Promise<Array>}
   */
  this.getMovies = function() {
    return $http.get(AppSettings.moviesServiceUrl)
        .then((response) => response.data.movies);
  };
}

export default {
  name: 'MoviesService',
  fn: MoviesService
};
