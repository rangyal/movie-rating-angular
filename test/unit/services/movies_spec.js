'use strict';

describe('Unit: MoviesService', function() {

  let http, service;

  beforeEach(function() {
    angular.mock.module('app');

    angular.mock.inject(($httpBackend, MoviesService) => {
      http = $httpBackend;
      service = MoviesService;
    });
  });

  it('should exist', function() {
    expect(service).toBeDefined();
  });

  it('should retrieve data', function(done) {
    http.expect('GET', 'https://gist.githubusercontent.com/razh/c37558e72a30bd836ec8f5192506f54a/raw/158671f939a695289685e8c3dd420ead4684ebda/movies.json')
      .respond(200, {
        movies: ['movie1', 'movie2']
      });

    service.getMovies().then((result) => {
      expect(result).toEqual(['movie1', 'movie2']);
    }).catch((error) => {
      expect(error).toBeUndefined();
    }).then(done);

    http.flush();
  });
});
