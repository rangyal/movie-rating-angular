'use strict';

describe('Unit: MoviesCtrl', function() {

  let ctrl, scope, moviesServiceSpy;

  beforeEach(function() {
    angular.mock.module('app');

    angular.mock.inject(($rootScope, $controller, $q) => {
      moviesServiceSpy = jasmine.createSpyObj('MoviesService', ['getMovies']);
      moviesServiceSpy.getMovies.and.returnValue($q.when(['movie1', 'movie2']));

      scope = $rootScope.$new();
      ctrl = $controller('MoviesCtrl', {$scope: scope, MoviesService: moviesServiceSpy});
    });
  });

  it('should exist', function() {
    expect(ctrl).toBeDefined();
  });

  it('should have MovieService.getMovies method called', function() {
    expect(moviesServiceSpy.getMovies).toHaveBeenCalled();
  });

  it('should have a \'list\' variable empty before initialization', function() {
    expect(ctrl.list).toBeUndefined();
  });

  it('should have a \'list\' variable with an array of movies after initialization', function() {
    scope.$root.$digest();
    expect(ctrl.list).toEqual(['movie1', 'movie2']);
  });

});
