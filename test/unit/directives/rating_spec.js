'use strict';

import Rating from './rating_spec_helper';

describe('Unit: RatingDirective', function() {

  [3, 5].forEach(function(maxValue) {
    it('should have ' + maxValue + ' star(s) if max-value attribute is ' + maxValue, function() {
      expect(new Rating(maxValue).starsCount).toBe(maxValue);
    });
  });

  [1, 3, 5].forEach(function(value) {
    it('should have ' + value + ' star(s) selected if "value" attribute is ' + value, function() {
      expect(new Rating(5, value).selectedStarsCount).toBe(value);
    });
  });

  [1, 2, 5].forEach(function(value) {
    it('should have ' + value + ' star(s) selected when star #' + value + ' is clicked', function() {
      let rating = new Rating(5);

      rating.clickStar(value);
      expect(rating.selectedStarsCount).toBe(value);
    });
  });

  [1, 4, 5].forEach(function(value) {
    it('should have ' + value + ' star(s) selected when changing the selection from 2 to ' + value + ' by clicking', function() {
      let rating = new Rating(5, 2);

      rating.clickStar(value);
      expect(rating.selectedStarsCount).toBe(value);
    });
  });

});
