'use strict';

class Rating {

  constructor(maxValue, value) {
    let element, scope;

    angular.mock.module('app');

    angular.mock.inject(($compile, $rootScope) => {
      scope = $rootScope;
      scope.value = value;

      element = angular.element(
        '<rating max-value="' + maxValue + '" value="value"></rating>'
      );

      $compile(element)(scope);
      scope.$digest();
    });

    this.element = element;
    this.scope   = scope;
  }

  get starsCount() {
    return this.element.find('.rating__star').length;
  }

  get selectedStarsCount() {
    return this.element.find('.rating__star--active').length;
  }

  clickStar(index) {
    angular.element(this.element.find('.rating__star').get(index - 1)).click();
    this.scope.$digest();
  }

}

export default Rating;
