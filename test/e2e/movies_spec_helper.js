class MoviesPage {

  constructor() {
    browser.get('/');
    browser.waitForAngular();

    let movies = $$('.movie');

    this.heading     = $('.heading').getText();
    this.moviesCount = movies.count();
    this.firstMovie  = new Movie(movies.first());
  }

}

class Movie {

  constructor(element) {
    this.element  = element;
    this.title    = element.$('h1').getText();
    this.synopsis = element.$('p').getText();
    this.rating   = new Rating(element.$('rating'));
  }

}

class Rating {

  constructor(element) {
    this._stars = element.$$('.rating__star');

    this.element    = element;
    this.starsCount = this._stars.count();
  }

  selectStarAndWaitForAnimation(index) {
    this._stars.get(index).click();
    browser.sleep((index + 1) * 500);
  }

  isStarSelectedAt(index) {
    return browser.executeScript(
      function isStarSelected() {
        switch (window.getComputedStyle(arguments[0], ':before').content.charCodeAt(1).toString(16)) {
          case 'f2fc':
            return true;
          case 'f3ae':
            return false;
        }
      },
      this._stars.get(index).getWebElement()
    );
  }

}

export default MoviesPage;
