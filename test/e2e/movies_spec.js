'use strict';

import _ from 'lodash';
import MoviesPage from './movies_spec_helper';

describe('E2E: Movies', function() {

  let page = new MoviesPage();

  it('should route correctly', function() {
    expect(browser.getLocationAbsUrl()).toMatch('/');
  });

  it('should have a heading', function() {
    expect(page.heading).toBe('Movies');
  });

  it('should have a list of movies', function() {
    expect(page.moviesCount).toBe(11);
  });

  describe('Movie item', function() {

    let movie = page.firstMovie;

    it('should have a title', function() {
      expect(movie.title).toBe('The Secret Life of Pets');
    });

    it('should have a truncated synopsis', function() {
      expect(movie.synopsis).toBe('For their fifth fully-animated feature-film collaboration, Illumination Entertainment and Universal…');
    });

    describe('Rating', function() {

      let rating = movie.rating;

      it('should be displayed', function() {
        expect(rating.element.isDisplayed()).toBe(true);
      });

      it('should have 5 stars', function() {
        expect(rating.starsCount).toBe(5);
      });

      it('should have selected stars highlighted only', function() {

        _.range(0, 4).forEach(function(index) {
          rating.selectStarAndWaitForAnimation(index);

          _.range(0, index).forEach(function(index) {
            expect(rating.isStarSelectedAt(index)).toBe(true);
          });

          index < 4 && _.range(index + 1, 4).forEach(function(index) {
            expect(rating.isStarSelectedAt(index)).toBe(false);
          });

        });
      });

    });

  });

});
