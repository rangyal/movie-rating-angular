'use strict';

import config from '../config';
import gulp   from 'gulp';
import sassLint from 'gulp-sass-lint';

gulp.task('sasslint', () => {
  return gulp.src(config.styles.src)
    .pipe(sassLint({
      configFile: '.sass-lint.yml'
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
});
