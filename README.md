Movie Rating - Angular ([http://r4ngy4l.gitlab.io/movie-rating-angular](http://r4ngy4l.gitlab.io/movie-rating-angular))
=================================================================================================================

### Getting up and running

1. Clone this repo
2. Run `npm install` from the root directory
3. Run `gulp dev` (may require installing Gulp globally `npm install gulp -g`)
4. Your browser will automatically be opened and directed to the browser-sync proxy address
5. To prepare assets for production, run the `gulp prod` task

### Testing

All of the tests can be run at once with the command `npm test`. However, the tests are broken up into two main categories:

##### End-to-End (e2e) Tests

All e2e tests are run with `npm run protractor`.

##### Unit Tests

All unit tests are run with `npm run unit`. When running unit tests, code coverage is simultaneously calculated and output as an HTML file to the `/coverage` directory.
